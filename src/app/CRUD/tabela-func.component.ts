import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { TabelaFuncDataSource, TabelaFuncItem } from './tabela-func-datasource';


@Component({
  selector: 'SIAR-tabela-func',
  templateUrl: './tabela-func.component.html',
  styleUrls: ['./tabela-func.component.css']
})
export class TabelaFuncComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatTable, {static: false}) table: MatTable<TabelaFuncItem>;
  dataSource: TabelaFuncDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['nome', 'email', 'funcao'];

  ngOnInit() {
    this.dataSource = new TabelaFuncDataSource();
    
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.table.dataSource = this.dataSource;
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
