import {MatTableDataSource} from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { map } from 'rxjs/operators';
import { Observable, of as observableOf, merge } from 'rxjs';

export interface TabelaFuncItem {
  nome: string;
  funcao: string;
  email?: string;
}

const EXAMPLE_DATA: TabelaFuncItem[] = [
  {nome: 'Carlos', email: 'xxx@xxx.com', funcao: 'garcom'},
  {nome: 'Amelia', email: 'amelia@gmail.com', funcao: 'cozinheira'},
];

export class TabelaFuncDataSource extends MatTableDataSource<TabelaFuncItem> {
  data: TabelaFuncItem[] = EXAMPLE_DATA;
  paginator: MatPaginator;
  sort: MatSort;

  constructor() {
    super();
  }

  
  disconnect() {}

  private getPagedData(data: TabelaFuncItem[]) {
    const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
    return data.splice(startIndex, this.paginator.pageSize);
  }

  private getSortedData(data: TabelaFuncItem[]) {
    if (!this.sort.active || this.sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      const isAsc = this.sort.direction === 'asc';
      switch (this.sort.active) {
        case 'nome': return compare(a.nome, b.nome, isAsc);
        case 'email': return compare(a.email, b.email, isAsc);
        case 'funcao': return compare(+a.funcao, +b.funcao, isAsc);
        default: return 0;
      }
    });
  }
}

function compare(a, b, isAsc) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
