import {Routes} from '@angular/router'
import { TabelaFuncComponent } from './CRUD/tabela-func.component'
import { FormularioComponent } from './funcionario/formulario/formulario.component'
import { HomeComponent } from './home/home.component'
export const ROUTES: Routes= [
    {path: '', component: HomeComponent},
    {path: 'cadastro', component: FormularioComponent },
    {path: 'CRUD', component: TabelaFuncComponent}
]