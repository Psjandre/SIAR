import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule} from '@angular/material/button';
import {MatCheckboxModule} from '@angular/material/checkbox';


const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forRoot(routes),
    NoopAnimationsModule,
    MatButtonModule,
    MatCheckboxModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
