import { Component, OnInit } from '@angular/core';
import {Funcionarios } from '../funcionarios';
import { FormGroup, FormControl } from '@angular/forms';
@Component({
  selector: 'SIAR-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {
  formFuncionario: FormGroup;

  constructor() { }

  ngOnInit() {
    this.createForm(new Funcionarios());
  }
  createForm(funcionarios: Funcionarios) {
    this.formFuncionario = new FormGroup({
      nome: new FormControl(funcionarios.nome),
      email: new FormControl(funcionarios.email),
      tipo: new FormControl(funcionarios.tipo),
      genero: new FormControl(funcionarios.genero),
      dataNascimento: new FormControl(funcionarios.dataNascimento),
      observacao: new FormControl(funcionarios.observacao),
      inativo: new FormControl(funcionarios.inativo)
    })

}
onSubmit() {
  // aqui você pode implementar a logica para fazer seu formulário salvar
  console.log(this.formFuncionario.value);

  // Usar o método reset para limpar os controles na tela
  this.formFuncionario.reset(new Funcionarios());
}
}
