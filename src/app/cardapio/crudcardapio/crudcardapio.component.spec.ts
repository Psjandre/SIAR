import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrudcardapioComponent } from './crudcardapio.component';

describe('CrudcardapioComponent', () => {
  let component: CrudcardapioComponent;
  let fixture: ComponentFixture<CrudcardapioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrudcardapioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudcardapioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
